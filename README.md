# Programacion Practica III
1. OPERADORES DE CONTROL:

Crear un programa que pida al usuario un n�mero entero y diga si es par.
Crear un programa que pida al usuario dos n�meros enteros y diga cu�l es el
mayor de ellos
Crear un programa que pida al usuario dos n�meros enteros y diga si el primero
es m�ltiplo del segundo
Crear un programa que pida al usuario un n�mero entero. Si es m�ltiplo de 10,
se lo avisar� al usuario y pedir� un segundo n�mero, para decir a continuaci�n
si este segundo n�mero tambi�n es m�ltiplo de 10.
Crear un programa que multiplique dos n�meros enteros de la siguiente forma:
pedir� al usuario un primer n�mero entero. Si el n�mero que se que teclee es 0,
escribir� en pantalla &quot;El producto de 0 por cualquier n�mero es 0&quot;. Si se ha
tecleado un n�mero distinto de cero, se pedir� al usuario un segundo n�mero y
se mostrar� el producto de ambos.
Crear un programa que pida al usuario dos n�meros enteros. Si el segundo no
es cero, mostrar� el resultado de dividir entre el primero y el segundo. Por el
contrario, si el segundo n�mero es cero, escribir� &quot;Error: No se puede dividir
entre cero&quot;.
Crear un programa que pida al usuario un n�mero enteros y diga si es m�ltiplo
de 4 o de 5.
Crear un programa que pida al usuario dos n�meros enteros y diga &quot;Uno de los
n�meros es positivo&quot;, &quot;Los dos n�meros son positivos&quot; o bien &quot;Ninguno de los
n�meros es positivo&quot;, seg�n corresponda.
Crear un programa que pida al usuario tres n�meros reales y muestre cu�l es el
mayor de los tres.

Practica #3