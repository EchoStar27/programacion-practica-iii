/*Crear un programa que pida al usuario dos n�meros enteros y diga si el primero
es m�ltiplo del segundo*/
using System;
class program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Programa para verificar que si el primer numero es multiplo del segundo");
        int num1;
        int num2;
        Console.WriteLine("");

        Console.Write("Digite el primer numero");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Digite el segundo numero");
        num2 = Convert.ToInt32(Console.ReadLine());

        if (num1%num2==0)
        {
            Console.WriteLine($"El primer numero, {num1}, es multiplo del numero {num2}");
        }
        else
        {
            Console.WriteLine($"El primer numero, {num1}, no es multiplo del numero {num2}");
        }

        Console.WriteLine("Programa finalizado");
    }
}