/*Crear un programa que pida al usuario dos n�meros enteros. Si el segundo no
es cero, mostrar� el resultado de dividir entre el primero y el segundo. Por el
contrario, si el segundo n�mero es cero, escribir� "Error: No se puede dividir
entre cero".*/
using System;

class Program
{
	static void Main(string[] args)
    {
        
        int num1;
        int num2;
        Console.WriteLine("Programa para dividir dos numeros enteros");
        
        Console.Write("Digite el primer numero: ");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Digite el segundo numero: ");
        num2 = Convert.ToInt32(Console.ReadLine());

        if (num2==0)
        {
            Console.WriteLine("Error: No se puede dividir entre cero");
        }
        else 
        {
            Console.WriteLine($"El resultado de dividir {num1} y {num2} es igual a: " + (num1 / num2));      
        }
      
    }
}