/*Crear un programa que pida al usuario tres n�meros reales y muestre cu�l es el
mayor de los tres.*/
using System;

class Program
{
    static void Main(string[] args)
    {
        float num1, num2, num3,mayor;
        Console.WriteLine("Programa para declarar cual es el mayor de 3 numero reales");

        Console.Write("Ingrese el primer numero:");
        num1 = Convert.ToSingle(Console.ReadLine());

        Console.Write("Ingrese el segundo numero: ");
        num2 = Convert.ToSingle(Console.ReadLine());

        Console.Write("Ingrese el tercer numero: ");
        num3 = Convert.ToSingle(Console.ReadLine());


        if (num1 > num2 && num1 > num3)
        {
            
            mayor = num1;
        }

        else if (num2 > num1 && num2 > num3)
        {
            mayor = num2;
        }
        else
        {
            mayor = num3;
        }
        Console.WriteLine($"El numero mayor es {mayor}");
    }
}
