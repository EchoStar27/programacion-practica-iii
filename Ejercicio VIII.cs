/*Crear un programa que pida al usuario dos n�meros enteros y diga Uno de los
n�meros es positivo o Los dos n�meros son positivos; o bien Ninguno de los
n�meros es positivo, seg�n corresponda.*/
using System;

class Program
{
	static void Main(string[] args)
    {
        Console.WriteLine("Programa para saber si los 2 numeros digitados son positivos");
        int num1,num2;

        Console.Write("Ingrese el primer numero: ");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Ingrese el segundo numero: ");
        num2 = Convert.ToInt32(Console.ReadLine());

        if (num1 > 0 && num2 > 0)
        {
            Console.WriteLine("Los dos numeros son positivos");
        }
        else if (num1 > 0 && num2 < 0)
        {
            Console.WriteLine("El primer numero es positivo y el segundo no");
        }
        else if (num1 < 0 && num2 > 0)
        {
            Console.WriteLine("El primer numero es negativo y el segundo es positivo");
        }
        else
        {
            Console.WriteLine("Ninguno de los dos son negativos.");
        }

        Console.WriteLine("El programa ha finalizado");
    }

}