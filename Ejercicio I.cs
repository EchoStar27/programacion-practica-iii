//Crear un programa que pida al usuario un n�mero entero y diga si es par.
using System;

class Program
{
    public static void Main(string[] args)
    {
        // Solo puede llevar numeros enteros
        int num1;

        Console.WriteLine("Programa para validar si un numero es par o impar");

        Console.Write("Ingrese el  Numero: ");
        num1 = Convert.ToInt32(Console.ReadLine());

        if (num1 % 2 == 0)
        {
            Console.WriteLine($"El numero {num1} es par ");
        }
        else
        {
            Console.WriteLine($"El numero {num1}  es impar");
        }

        Console.WriteLine("El programa ha finalizado");



    }
}
