/*Crear un programa que multiplique dos n�meros enteros de la siguiente forma:
pedir� al usuario un primer n�mero entero. Si el n�mero que se que teclee es 0,
escribir� en pantalla El producto de 0 por cualquier n�mero es 0;. Si se ha
tecleado un n�mero distinto de cero, se pedir� al usuario un segundo n�mero y
se mostrar� el producto de ambos.*/
using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Programa para multiplicar dos numeros");
        int num1;
        Console.Write("Digite su primer numero: ");
        num1 =Convert.ToInt32(Console.ReadLine());

        if (num1 == 0)
        {
            Console.WriteLine("Recuerde que el producto de un numero multiplicado por 0 es igual a 0");

        }
        else
        {
            Console.Write("Ingrese el segundo numero: ");
            int num2=Convert.ToInt32(Console.ReadLine());

           if (num2==0)
            {
                Console.WriteLine("Recuerde que el producto de un numero multiplicado por 0 es igual a 0");
            }
           else
            {
                Console.WriteLine($"El resultado de los productos {num1} y {num2} es : " + (num1 * num2));
            }

        }

        Console.WriteLine("El programa ha finalizado");
    } 

}






