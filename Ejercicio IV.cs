/*Crear un programa que pida al usuario un n�mero entero. Si es m�ltiplo de 10,
se lo avisar� al usuario y pedir� un segundo n�mero, para decir a continuaci�n
si este segundo n�mero tambi�n es m�ltiplo de 10.*/
using System;

class program
{
    static void Main(string[] args)
    {
        int num;
        Console.Write("Digite el numero: ");
        num = Convert.ToInt32(Console.ReadLine());

        if (num % 10 == 0)
        {
            Console.WriteLine($"El numero {num} ingresado es multiplo de 10");
            Console.Write("Ingrese otro  numero: ");

            num = Convert.ToInt32(Console.ReadLine());

            if (num % 10 == 0)
            {
                Console.WriteLine($"El numero {num} es tambien multiplo de 10 ");
            }
            else
            {
                Console.WriteLine("El segundo numero no es multiplo de 10");
            }

        }
        else 
        {
            Console.WriteLine($"El numero {num} no es multiplo de 10");
        } 
       
        Console.WriteLine("El programa ha finalizado");



    }
}