/*Crear un programa que pida al usuario un n�mero enteros y diga si es m�ltiplo
de 4 o de 5.*/

using System;

class Program
{
    static void Main(string[] args)
    {
        int num;
        Console.WriteLine("Programa para verificar si el numero ingresado es multiplo de 4 o de 5");

        Console.Write("Ingrese el numero que verificara: ");
        num = Convert.ToInt32(Console.ReadLine());

        if (num % 4 == 0)
        {
            if(num%5==0)
            {
                Console.WriteLine($"El numero {num} es multiplo de 4 y de 5");
            }
            else
            {
                Console.WriteLine($"El numero {num} es multiplo de 4");

            }
       
        }
        else if (num%5==0)
        {
            if (num%4==0)
            {
                Console.WriteLine($"El numero {num} es multiplo de 5 y de 4");
            }
            else
            {
                Console.WriteLine($"El numero {num} es multiplo de 5");

            }
            
        }
        else
        {
            Console.WriteLine($"El numero {num} no es ni multiplo de 4 ni de 5");
        }

        Console.WriteLine("El programa ha finalizado"); 
    }
}