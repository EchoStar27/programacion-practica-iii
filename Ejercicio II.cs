/*Crear un programa que pida al usuario dos n�meros enteros y diga cu�l es el
mayor de ellos*/
using System;

class program
{
    static void Main(string[] args)
    {
        int num1;
        int num2;

        Console.WriteLine("Programa para decir cual es el mayor de dos numeros");
        Console.Write("Digite el primer numero");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Digite el segundo numero");
        num2 = Convert.ToInt32(Console.ReadLine());

        if (num1 > num2)
        {
            Console.WriteLine($"El numero {num1} es mayor que {num2} ");
        }
        else 
        {
            Console.WriteLine($"El numero {num2} es mayor {num1} ");
        }
    }
}